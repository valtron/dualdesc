import numpy as np
import dualdesc as dd

def test_basic():
	M = np.array([
		[1, 0, 1], # x0 <= 1
		[0, 1, 1], # x1 <= 1
		[1, 1, 1], # x0 + x1 <= 1
	], dtype = np.float64)
	p1 = dd.Polytope.FromHalfspaces(M[:,:2], M[:,-1])
	Vc, Vn = p1.get_generators()
	assert _vectors_equal(Vc, np.eye(2), eps = 1e-5)
	assert _cones_equal(Vn, -np.eye(2), eps = 1e-5)

def test_consistency():
	rng = np.random.default_rng(0)
	for _ in range(10):
		n = rng.integers(1, 15)
		d = rng.integers(2, 6)
		p1 = dd.Polytope.FromGenerators(rng.uniform(size = (n, d)), scale = 1e6)
		p2 = dd.Polytope.FromHalfspaces(*p1.get_inequalities(), scale = 1e6)
		Vc, Vn = p2.get_generators()
		assert len(Vn) == 0
		assert _vectors_subset(p1.get_generators()[0], Vc, eps = 1e-4)

def _cones_equal(C1, C2, *, eps):
	C1 = C1 / np.linalg.norm(C1, axis = 1, keepdims = True)
	C2 = C2 / np.linalg.norm(C2, axis = 1, keepdims = True)
	return _vectors_equal(C1, C2, eps = eps)

def _vectors_equal(V1, V2, *, eps):
	D = (np.abs(np.linalg.norm(V1[:,:,None] - V2.T[None,:,:], axis = 1)) <= eps)
	if not np.all(np.any(D, axis = 0)):
		return False
	if not np.all(np.any(D, axis = 1)):
		return False
	return True

def _vectors_subset(V1, V2, *, eps):
	D = (np.abs(np.linalg.norm(V1[:,:,None] - V2.T[None,:,:], axis = 1)) <= eps)
	if not np.all(np.any(D, axis = 1)):
		return False
	return True
