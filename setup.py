from pathlib import Path
import setuptools

package_name = 'dualdesc'

setuptools.setup(
	name = package_name,
	version = '0.2.0',
	author = "valtron",
	description = "Dual description of polytopes.",
	long_description = Path('README.md').read_text(),
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/valtron/{}'.format(package_name),
	packages = [package_name],
	package_data = { package_name: ['py.typed'] },
	python_requires = '>=3.9',
	install_requires = ['pyparma ~= 0.5.0', 'numpy ~= 1.25.2'],
	setup_requires = ['pytest-runner'],
	tests_require = ['pytest', 'pytest-cov'],
	classifiers = [
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.9',
		'Programming Language :: Python :: 3.10',
		#'Programming Language :: Python :: 3.11',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
	],
)
